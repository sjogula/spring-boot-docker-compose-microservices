package com.example.springbooteurekaconsumer;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableEurekaClient
@EnableFeignClients
public class SpringBootEurekaConsumerApplication {

	@Autowired
	private UserProxyService userProxyService;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEurekaConsumerApplication.class, args);
	}

	@GetMapping("/api/users")
	public List<User> getUsers4mDB() {
		return userProxyService.getUsers4mDB();
	}
}
