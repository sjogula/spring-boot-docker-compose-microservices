package com.example.springbooteurekaconsumer;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "SPRING-BOOT-PRODUCER")
public interface UserProxyService {
	@GetMapping("/users")
	public List<User> getUsers4mDB();
}
