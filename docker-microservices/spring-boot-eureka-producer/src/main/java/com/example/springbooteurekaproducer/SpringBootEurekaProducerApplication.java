package com.example.springbooteurekaproducer;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableEurekaClient
public class SpringBootEurekaProducerApplication {

	@Autowired
	private UserRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEurekaProducerApplication.class, args);
	}

	@GetMapping()
	public String save() {
		List<User> users = new ArrayList<>();
		users.add(new User("Shekhar", "Architect"));
		users.add(new User("Jogula", "Programmer"));
		repository.saveAll(users);
		return "welcome";
	}

	@GetMapping("users")
	public List<User> getUsers4mDB() {
		return repository.findAll();
	}

}
